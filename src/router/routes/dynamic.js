/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-22 14:24:05
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/router/routes/dynamic.js
 * @Just do what I think it is right
 */
const dynamic = [
    {
        name: 'auth_dynamic',
        path: '/auth_dynamic',
        component: () => import('@/layout/auth/index'),
        children: [
            {
                name: 'merchantList',
                path: '/merchant/list',
                meta: {
                    title: '商户列表',
                    requiresAuth: true,
                    ruleId: 15
                },
                component: () => import('@/pages/merchant/list')
            },
            {
                name: 'merchantAdd',
                path: '/merchant/add',
                meta: {
                    title: '新增商户',
                    requiresAuth: true,
                    ruleId: 15
                },
                component: () => import('@/pages/merchant/add')
            },
            {
                name: 'merchantInfo',
                path: '/merchant/info',
                meta: {
                    title: '商户详情',
                    requiresAuth: true,
                    ruleId: -1
                },
                component: () => import('@/pages/merchant/info')
            },
            {
                name: 'merchantUpdate',
                path: '/merchant/update',
                meta: {
                    title: '商户更新',
                    requiresAuth: true,
                    ruleId: -1
                },
                component: () => import('@/pages/merchant/update')
            },
            {
                name: 'goodsList',
                path: '/goods/list',
                meta: {
                    title: '商品列表',
                    requiresAuth: true,
                    ruleId: 16
                },
                component: () => import('@/pages/goods/list')
            },
            {
                name: 'goodsInfo',
                path: '/goods/info',
                meta: {
                    title: '商品详情',
                    requiresAuth: true,
                    ruleId: -1
                },
                component: () => import('@/pages/goods/info')
            },
            {
                name: 'goodsCategoryList',
                path: '/goods/category/list',
                meta: {
                    title: '商品分类列表',
                    requiresAuth: true,
                    ruleId: 17
                },
                component: () => import('@/pages/goods/category/list')
            },
            {
                name: 'goodsBrandList',
                path: '/goods/brand/list',
                meta: {
                    title: '商品品牌列表',
                    requiresAuth: true,
                    ruleId: 18
                },
                component: () => import('@/pages/goods/brand/list')
            },
            {
                name: 'memberList',
                path: '/member/list',
                meta: {
                    title: '会员列表',
                    requiresAuth: true,
                    ruleId: 18
                },
                component: () => import('@/pages/member/list')
            },
            {
                name: 'memberGrowthList',
                path: '/member/growth/list',
                meta: {
                    title: '积分规则',
                    requiresAuth: true,
                    ruleId: -1
                },
                component: () => import('@/pages/member/growth/list')
            },
            {
                name: 'authUserList',
                path: '/auth/user/list',
                meta: {
                    title: '用户列表',
                    requiresAuth: true,
                    ruleId: 19
                },
                component: () => import('@/pages/auth/user/list')
            },
            {
                name: 'authRoleList',
                path: '/auth/role/list',
                meta: {
                    title: '角色列表',
                    requiresAuth: true,
                    ruleId: 20
                },
                component: () => import('@/pages/auth/role/list')
            },
            {
                name: 'authRuleList',
                path: '/auth/rule/list',
                meta: {
                    title: '权限列表',
                    requiresAuth: true,
                    ruleId: 21
                },
                component: () => import('@/pages/auth/rule/list')
            }
        ]
    }
]

export default dynamic 
