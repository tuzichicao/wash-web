/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-13 14:55:05
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/router/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import { init } from './routes/init'


Vue.use(VueRouter)

export const router = new VueRouter({
    mode: 'history',
    routes: init
})