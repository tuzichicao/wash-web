/*
 * @Author: 码上talk|RC
 * @Date: 2021-01-21 14:28:35
 * @LastEditTime: 2021-01-21 16:42:40
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/components/popup/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/* eslint-disable */
import Vue from 'vue'
import popupComponent from './index.vue'


const PopupConstructor = Vue.extend(popupComponent)
let popupPool = []

let getAnInstance = () => {
    if (popupPool.length > 0) {
        let instance = popupPool[0]
        popupPool.splice(0, 1)
        return instance
    }
    return new PopupConstructor({
        el: document.createElement('div')
    })
}

let returnAnInstance = instance => {
    if (instance) {
        popupPool.push(instance)
    }
}

let removeDom = event => {
    if (event.target.parentNode) {
        event.target.parentNode.removeChild(event.target)
    }
}

PopupConstructor.prototype.close = function () {
    this.visible = false
    this.$el.addEventListener('transitionend', removeDom)
    returnAnInstance(this)
}


const popup = (tpl, config = {}) => {
    let instance = getAnInstance()
    document.body.appendChild(instance.$el)
    Vue.nextTick(function () {
        instance.visible = true
        instance.tpl = tpl
        instance.model = config.model
    })
    return instance
}

export default popup