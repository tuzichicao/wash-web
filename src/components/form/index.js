/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-18 14:19:40
 * @LastEditTime: 2020-12-18 14:20:38
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/components/form/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import box from './packages/box'
import item from './packages/item'

export const formBox = box
export const formItem = item