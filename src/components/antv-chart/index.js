/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-18 09:10:57
 * @LastEditTime: 2020-12-18 10:06:40
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/components/antv-chart/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import line from './packages/line'
import interval from './packages/interval'

export const antvLine = line
export const antvInterval = interval