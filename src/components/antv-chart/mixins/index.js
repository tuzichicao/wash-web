/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-18 14:31:42
 * @LastEditTime: 2021-01-11 14:48:51
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/components/antv-chart/mixins/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/*eslint-disable */
import F2 from '@antv/f2'
import { fn } from '@/utils/fn'

export const mixins = {
    props: ['data', 'defs'],
    data() {
        return {
            cId: `${this.$options.name}_${this._uid}`,
            chart: null,
            width: 0,
            height: 0,
            debounceRender: fn.debounce(this.resizeRender, null, 500)
        }
    },
    methods: {
        listenResize() {
            window.addEventListener('resize', () => {
                this.fixedWidthAndHeight()
                this.debounceRender()
            })
        },
        fixedWidthAndHeight() {
            this.width = Number(window.getComputedStyle(this.$el).width.split('px')[0])
            this.height = Number(window.getComputedStyle(this.$el).height.split('px')[0])
        },
        ready() {
            this.chart = new F2.Chart(Object.assign({
                id: this.cId,
                pixelRatio: window.devicePixelRatio,
                width: this.width,
                height: this.height,
            }, this.config['chart']))
            this.chart.source(this.data, this.defs);
            this.chart.axis(false)
            if (this.config['axis']) {
                this.chart.axis('x', this.config['axis'])
            }
            this.render(this.chart)
        },
        resizeRender() {
            this.chart.changeSize(this.width, this.height)
        }
    },
    mounted() {
        this.fixedWidthAndHeight()
        this.ready()
        this.listenResize()
    }
}