/*
 * @Author: 码上talk|RC
 * @Date: 2020-11-10 14:45:05
 * @LastEditTime: 2021-01-20 14:17:56
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/api/modules/auth.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { http } from '@/libs/http'

export const auth = {
    userList: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/auth/adminUserPage',
            params,
            data
        })
    },
    roleList: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/auth/adminAuthRolePage',
            params,
            data
        })
    },
    ruleList: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/auth/adminAuthRulePage',
            params,
            data
        })
    }
}