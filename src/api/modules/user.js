/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-12 15:51:50
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/api/modules/user.js
 * @Just do what I think it is right
 */
import { http } from '@/libs/http'

export const user = {
    login: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/user/login',
            params,
            data
        })
    },
    info: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/user/info',
            params,
            data
        })
    },
    rules: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/user/rules',
            params,
            data
        })
    },
    logout: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/user/logout',
            params,
            data
        })
    }
}
