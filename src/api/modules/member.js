/*
 * @Author: 码上talk|RC
 * @Date: 2021-01-20 15:09:35
 * @LastEditTime: 2021-01-20 15:09:49
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/api/modules/member.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { http } from '@/libs/http'

export const member = {
    list: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/member/page',
            params,
            data
        })
    },
    info: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/member/info',
            params,
            data
        })
    }
}