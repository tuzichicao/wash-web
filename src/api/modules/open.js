/*
 * @Author: 码上talk|RC
 * @Date: 2020-11-17 09:30:18
 * @LastEditTime: 2020-11-17 09:39:53
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-merchant/src/api/modules/open.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { http } from '@/libs/http'
import { appConfig } from '@/config/app'

let openUrl = appConfig.openUrl.dev

if (process.env.NODE_ENV === 'production') {
    openUrl = appConfig.openUrl.prod
}

export const open = {
    enumLst: (params = {}, data = {}) => {
        return http.request({
            url: '/open/enum/lst',
            params,
            data,
            baseURL: openUrl
        })
    }
}
