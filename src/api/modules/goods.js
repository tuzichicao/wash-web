/*
 * @Author: 码上talk|RC
 * @Date: 2020-11-10 14:45:05
 * @LastEditTime: 2021-01-20 10:58:59
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/api/modules/goods.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { http } from '@/libs/http'

export const goods = {
    list: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/goods/page',
            params,
            data
        })
    },
    info: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/goods/info',
            params,
            data
        })
    },
    categoryList: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/goods/goodsCategoryPage',
            params,
            data
        })
    },
    brandList: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/goods/goodsBrandPage',
            params,
            data
        })
    }
}