/*
 * @Author: 码上talk|RC
 * @Date: 2021-01-20 10:28:33
 * @LastEditTime: 2021-01-20 16:54:33
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/api/modules/merchant.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { http } from '@/libs/http'

export const merchant = {
    list: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/merchant/page',
            params,
            data
        })
    },
    info: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/merchant/info',
            params,
            data
        })
    },
    add: (params = {}, data = {}) => {
        return http.request({
            url: '/admin/merchant/add',
            params,
            data
        })
    }
}