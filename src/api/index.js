/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-20 15:13:18
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/api/index.js
 * @Just do what I think it is right
 */
import { user } from './modules/user'
import { open } from './modules/open'
import { goods } from './modules/goods'
import { merchant } from './modules/merchant'
import { auth } from './modules/auth'
import { member } from './modules/member'

export default {
    user,
    open,
    goods,
    merchant,
    auth,
    member
}
