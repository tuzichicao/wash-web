/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-15 15:09:12
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/config/app.js
 * @Just do what I think it is right
 */
export const appConfig = {
    baseUrl: {
        dev: '/',
        prod: '//apitc.codingtalk.cn/'
    },
    openUrl: {
        dev: '/',
        prod: '//apitc.codingtalk.cn/'
    },
    tokenKey: 'user-access-token',
    ossUrl: {
        dev: '//codingtalk-imgtest.oss-cn-shenzhen.aliyuncs.com/',
        prod: '//img.codingtalk.cn/'
    }
}
