/*
 * @Author: 码上talk|RC
 * @Date: 2020-11-23 18:13:05
 * @LastEditTime: 2021-01-14 11:39:11
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/middleware/afterEach.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { router } from '@/router'
import { store } from '@/store'
import { historyBar } from '@/utils/history-bar'

router.afterEach((from) => {
    historyBar.add({
        name: from.meta.title,
        url: from.fullPath
    })
    store.commit('sys/SET_HISTORY_BAR', historyBar.all(), { root: true })
})