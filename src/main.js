/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-14 15:58:42
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/main.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import { store } from './store'
import { layout } from './layout'
import './middleware/beforeEach'
import './middleware/afterEach'
import './plugins/mixins'
import './assets/styles/index.less'
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'


Vue.use(layout)
Vue.use(ViewUI)
Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app')
