/*
 * @Author: 码上talk|RC
 * @Date: 2020-11-11 15:35:10
 * @LastEditTime: 2021-01-20 15:10:33
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/model/member/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import api from '@/api'
export class Member {
    constructor(member = {}) {
        const { id, nickname, avatar, gender } = member
        this.id = id
        this.nickname = nickname
        this.avatar = avatar
        this.gender = gender
    }
}

Member.ENUM = {
    init: () => {
        return {
            status: []
        }
    },
}

Member.API = (type = 'list', req) => {
    switch (type) {
        case 'list':
            return new Promise((resolve) => {
                api.member.list(req.params, req.data).then(res => {
                    const { status, data, page } = res
                    if (status) {
                        resolve({ data, page })
                    }
                })
            })
        case 'info':
            return new Promise((resolve) => {
                api.member.info(req.params, req.data).then(res => {
                    const { status, data } = res
                    if (status) {
                        resolve({ data })
                    }
                })
            })
        default:
            break
    }
}