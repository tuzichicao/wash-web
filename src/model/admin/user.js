/*
 * @Author: 码上talk|RC
 * @Date: 2021-01-20 14:09:14
 * @LastEditTime: 2021-01-20 14:30:40
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/model/admin/User.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import api from '@/api'

export class AdminUser {
    constructor(item = {}) {
        const { id, name, username } = item
        this.id = id
        this.name = name
        this.username = username
    }
}

AdminUser.ENUM = {
    init: () => {
        return {}
    }
}

AdminUser.API = (type = 'list', req) => {
    switch (type) {
        case 'list':
            return new Promise((resolve) => {
                api.auth.userList(req.params, req.data).then(res => {
                    const { status, data, page } = res
                    if (status) {
                        resolve({ data, page })
                    }
                })
            })
        default:
            break
    }
}
