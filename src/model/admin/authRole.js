/*
 * @Author: 码上talk|RC
 * @Date: 2020-10-24 15:02:14
 * @LastEditTime: 2021-01-20 14:15:03
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/model/admin/role.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import api from '@/api'

export class AdminAuthRole {
    constructor(item = {}) {
        const { id, name } = item
        this.id = id
        this.name = name
    }
}

AdminAuthRole.ENUM = {
    init: () => {
        return {}
    }
}

AdminAuthRole.API = (type = 'list', req) => {
    switch (type) {
        case 'list':
            return new Promise((resolve) => {
                api.auth.roleList(req.params, req.data).then(res => {
                    const { status, data, page } = res
                    if (status) {
                        resolve({ data, page })
                    }
                })
            })
        default:
            break
    }
}
