/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-13 10:00:35
 * @LastEditTime: 2021-01-22 11:40:11
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/model/merchant/user.js
 * @Just do what I think it is right
 */
export class MerchantUser {
    constructor(user = {}) {
        const { id, nickname, avatar } = user
        this.id = id
        this.nickname = nickname
        this.avatar = avatar
    }
}

MerchantUser.FORM = {
    init: (type = 'add') => {
        switch (type) {
            case 'add':
                return {
                    username: {
                        isRequire: true,
                        type: 'string|min:2',
                        key: 'username',
                        value: null
                    },
                    passwd: {
                        isRequire: false,
                        type: 'string|min:2',
                        key: 'passwd',
                        value: null
                    },
                    avatar: {
                        isRequire: false,
                        type: 'string|min:2',
                        key: 'avatar',
                        value: null
                    }
                }
            case 'update':
                return {
                    id: {
                        isRequire: true,
                        type: 'int',
                        key: 'id',
                        value: null
                    },
                    username: {
                        isRequire: true,
                        type: 'string|min:2',
                        key: 'username',
                        value: null
                    },
                    passwd: {
                        isRequire: false,
                        type: 'string|min:2',
                        key: 'passwd',
                        value: null
                    },
                    avatar: {
                        isRequire: false,
                        type: 'string|min:2',
                        key: 'avatar',
                        value: null
                    }
                }
            default:
                break
        }
    }
}