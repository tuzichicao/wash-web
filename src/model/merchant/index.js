/*
 * @Author: 码上talk|RC
 * @Date: 2020-11-11 15:35:10
 * @LastEditTime: 2021-01-22 11:06:59
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/model/merchant/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/*eslint-disable */
import api from '@/api'
import { formToolkit } from '@/utils/form-toolkit'
export class Merchant {
    constructor(merchant = {}) {
        const { id, name, avatar } = merchant
        this.id = id
        this.name = name
        this.avatar = avatar
    }
}

Merchant.ENUM = {
    init: () => {
        return {
        }
    },
    load: async () => {
        let result = {}
        for (let i in Merchant.ENUM.init()) {
            let body = {}
            const res = await api.open.enumLst({ key: i }, body)
            let { status, data } = res
            if (status) {
                result[i] = data.map(j => {
                    let json = {
                        value: j['id'],
                        label: j['name']
                    }
                    return json
                })
            }
        }
        return result
    },
}

Merchant.FORM = {
    init: (type = 'add') => {
        switch (type) {
            case 'add':
                return {
                    name: {
                        isRequire: true,
                        type: 'string|min:2',
                        key: 'name',
                        value: null
                    },
                    cover: {
                        isRequire: false,
                        type: 'string|min:2',
                        key: 'cover',
                        value: ''
                    },
                    mobile: {
                        isRequire: false,
                        type: 'string|min:2',
                        key: 'mobile',
                        value: null
                    },
                    merchantUsers: {
                        isRequire: true,
                        type: 'array>MerchantUser',
                        key: 'merchantUsers',
                        value: []
                    }
                }
            case 'update':
                return {
                    id: {
                        isRequire: true,
                        type: 'int',
                        key: 'id',
                        value: null
                    },
                    name: {
                        isRequire: true,
                        type: 'string|min:2',
                        key: 'name',
                        value: null
                    },
                    cover: {
                        isRequire: true,
                        type: 'string|min:2',
                        key: 'cover',
                        value: null
                    },
                    merchantUsers: {
                        isRequire: true,
                        type: 'array>MerchantUser',
                        key: 'merchantUsers',
                        value: []
                    }
                }
            default:
                break
        }
    },
    load: async (type = 'update', json) => {
        switch (type) {
            case 'update':
                const { data } = await Merchant.API('info', { params: { id: json['id'] }, data: {} })
                return { res: data, form: formToolkit.fill(Merchant.FORM.init('update'), data) }
            default:
                break
        }
    }
}

Merchant.API = (type = 'list', req) => {
    switch (type) {
        case 'list':
            return new Promise((resolve) => {
                api.merchant.list(req.params, req.data).then(res => {
                    const { status, data, page } = res
                    if (status) {
                        resolve({ data, page })
                    }
                })
            })
        case 'info':
            return new Promise((resolve) => {
                api.merchant.info(req.params, req.data).then(res => {
                    const { status, data } = res
                    if (status) {
                        resolve({ data })
                    }
                })
            })
        case 'add':
            return new Promise((resolve) => {
                api.merchant.add(req.params, req.data).then(res => {
                    const { status, data } = res
                    if (status) {
                        resolve({ data })
                    }
                })
            })
        default:
            break
    }
}