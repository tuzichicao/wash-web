/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-29 17:53:17
 * @LastEditTime: 2020-12-29 17:54:10
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-merchant/src/model/oss/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */


export class Oss {
    constructor(oss = {}) {
        const { id, filename } = oss
        this.id = id
        this.filename = filename
    }
}