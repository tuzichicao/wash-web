/*
 * @Author: 码上talk|RC
 * @Date: 2020-10-24 15:02:14
 * @LastEditTime: 2021-01-15 15:20:20
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/model/goods/item.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import api from '@/api'
export class GoodsItem {
    constructor(item = {}) {
        const { id, goodsId, name, cover, amount } = item
        this.id = id
        this.goodsId = goodsId
        this.cover = cover
        this.name = name
        this.amount = amount
    }
}

GoodsItem.ENUM = {
    init: () => {
        return {
            goodsAttr: []
        }
    },
    load: async (config = {}) => {
        let result = {}
        for (let i in GoodsItem.ENUM.init()) {
            let body = {}
            if (i === 'goodsAttr') {
                const query = {
                    goodsCategoryId: config['goodsCategoryId']
                }
                body['query'] = query
            }
            const res = await api.open.enumLst({ key: i }, body)
            let { status, data } = res
            if (status) {
                result[i] = data.map(j => {
                    let json = {
                        value: j['id'],
                        label: j['name']
                    }
                    if (i === 'goodsAttr') {
                        json = {
                            value: j['id'],
                            label: j['name'],
                            children: j['children'].map(k => {
                                return {
                                    value: k['id'],
                                    label: k['name']
                                }
                            })
                        }
                    }
                    return json
                })
            }
        }
        return result
    }
}
