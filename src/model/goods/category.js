/*
 * @Author: 码上talk|RC
 * @Date: 2020-10-24 15:02:14
 * @LastEditTime: 2021-01-20 11:29:12
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/model/goods/category.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import api from '@/api'

export class GoodsCategory {
    constructor(item = {}) {
        const { id, cover, name } = item
        this.id = id
        this.cover = cover
        this.name = name
    }
}

GoodsCategory.ENUM = {
    init: () => {
        return {}
    }
}

GoodsCategory.API = (type = 'list', req) => {
    switch (type) {
        case 'list':
            return new Promise((resolve) => {
                api.goods.categoryList(req.params, req.data).then(res => {
                    const { status, data, page } = res
                    if (status) {
                        resolve({ data, page })
                    }
                })
            })
        default:
            break
    }
}
