/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-13 10:00:35
 * @LastEditTime: 2021-01-20 10:28:11
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/model/goods/index.js
 * @Just do what I think it is right
 */
/*eslint-disable */
import { GoodsItem } from './item'
import api from '@/api'

export class Goods {
    constructor(goods = {
        goodsItem: []
    }) {
        const { id, name, amount, cover, goodsItem, stock } = goods
        this.id = id
        this.name = name
        this.amount = amount
        this.cover = cover
        this.items = goodsItem.map(i => new GoodsItem(i))
        this.status = '出售中'
        this.stock = 1000
    }
}

Goods.ENUM = {
    init: () => {
        return {
            goodsCategory: [],
            goodsBrand: []
        }
    },
    load: async () => {
        let result = {}
        for (let i in Goods.ENUM.init()) {
            let body = {}
            if (i === 'goodsCategory') {
                const query = {
                    pId: 0
                }
                body['query'] = query
            }
            const res = await api.open.enumLst({ key: i }, body)
            let { status, data } = res
            if (status) {
                result[i] = data.map(j => {
                    let json = {
                        value: j['id'],
                        label: j['name']
                    }
                    if (i === 'goodsCategory') {
                        json = {
                            value: j['id'],
                            label: j['name'],
                            children: [],
                            loading: false
                        }
                    }
                    return json
                })
            }
        }
        return result
    },
    lazyLoad: async (key = 'goodsCategory', body, config = {}) => {
        const res = await api.open.enumLst({ key }, body)
        let { status, data } = res
        if (status) {
            if (key === 'goodsCategory') {
                config['item'].children = data.map(i => {
                    let json = {
                        value: i['id'],
                        label: i['name'],
                        children: [],
                        loading: false
                    }
                    if (!i.isHasChildren) {
                        json = {
                            value: i['id'],
                            label: i['name'],
                        }
                    }
                    return json
                })
                config['item'].loading = false
                config['callback']()
            }
        }
    }
}


Goods.API = (type = 'list', req) => {
    switch (type) {
        case 'list':
            return new Promise((resolve) => {
                api.goods.list(req.params, req.data).then(res => {
                    const { status, data, page } = res
                    if (status) {
                        resolve({ data, page })
                    }
                })
            })
        case 'info':
            return new Promise((resolve) => {
                api.goods.info(req.params, req.data).then(res => {
                    const { status, data } = res
                    if (status) {
                        resolve({ data })
                    }
                })
            })
        default:
            break
    }
}