/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-20 14:40:48
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/store/sys/mutations.js
 * @Just do what I think it is right
 */

import * as types from './mutation-types'
const _ = require('lodash')
import menuJson from '@/config/menu.json'
import { router } from '@/router'
import dynamicRoutes from '@/router/routes/dynamic'

export const mutations = {
    [types.SET_MENU](state, ruleIds) {
        state.menu = _.cloneDeep(menuJson).filter(i => {
            if (i.ruleId && !ruleIds.includes(i.ruleId)) {
                return false
            }
            if (i.sub && i.sub.length) {
                i.sub = i.sub.filter(j => {
                    if (!j.ruleId) {
                        return true
                    }
                    if (j.ruleId && ruleIds.includes(j.ruleId)) {
                        return true
                    }
                    return false
                })
                if (!i.sub.length) {
                    return false
                }
            }
            return true
        })
    },
    [types.SET_IS_DYNAMIC_ROUTES_READY](state, ruleIds) {
        const routes = _.cloneDeep(dynamicRoutes)
        routes.forEach(i => {
            i.children = i.children.filter(j => {
                if (j.meta && j.meta.ruleId && !ruleIds.includes(j.meta.ruleId)) {
                    return false
                }
                return true
            })
        })
        router.addRoutes(routes)
        state.isDaynamicRoutesReady = true
    },
    [types.SET_HISTORY_BAR](state, a) {
        state.historyBar = a
    }
}
