/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-15 14:26:14
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/store/sys/actions.js
 * @Just do what I think it is right
 */

import api from '@/api'
import * as types from './mutation-types'

export const actions = {
    queryRules({ commit }) {
        return new Promise((resolve) => {
            api.user.rules().then(res => {
                const { status, data } = res
                if (status) {
                    const ruleIds = data.map(i => i.id)
                    commit(types.SET_MENU, ruleIds)
                    commit(types.SET_IS_DYNAMIC_ROUTES_READY, ruleIds)
                    resolve()
                }
            })
        })
    }
}
