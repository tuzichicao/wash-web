/*
 * @Author: 码上talk|RC
 * @Date: 2021-01-13 17:00:24
 * @LastEditTime: 2021-01-14 16:38:47
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/store/sys/mutation-types.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
export const SET_MENU = 'SET_MENU'
export const SET_IS_DYNAMIC_ROUTES_READY = 'SET_IS_DYNAMIC_ROUTES_READY'
export const SET_HISTORY_BAR = 'SET_HISTORY_BAR'
