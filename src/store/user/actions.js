/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-15 14:29:47
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/store/user/actions.js
 * @Just do what I think it is right
 */

import * as types from './mutation-types'
import api from '@/api'
import { session } from '@/libs/session'

export const actions = {
    getUserInfo({ commit }) {
        return new Promise((resolve) => {
            api.user.info().then(res => {
                const { status, data } = res
                if (status) {
                    commit(types.SET_PROFILE, data)
                    commit('setIsLogin', true, { root: true })
                    resolve()
                }
            })
        })
    },
    logout({ commit }) {
        return new Promise((resolve) => {
            api.user.info().then(res => {
                if (res.status) {
                    session.exit()
                    commit(types.SET_PROFILE, {})
                    commit('setIsLogin', false, { root: true })
                    resolve()
                }
            })
        })
    }
}
