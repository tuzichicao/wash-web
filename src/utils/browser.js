/*
 * @Author: 码上talk|RC
 * @Date: 2020-10-09 15:46:22
 * @LastEditTime: 2021-01-15 14:30:33
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/src/utils/browser.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/* eslint-disable */
export const fullScreen = () => {
    var el = document.documentElement
    var rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullScreen

    if (rfs) {
        rfs.call(el)
    }
    else if (typeof window.ActiveXObject !== 'undefined') {
        var wscript = new ActiveXObject("WScript.Shell")
        if (wscript != null) {
            wscript.SendKeys("{F11}")
        }
    }
}

export const exitFullScreen = () => {
    var el = document
    var cfs = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullScreen

    if (cfs) {
        cfs.call(el)
    }
    else if (typeof window.ActiveXObject !== 'undefined') {
        var wscript = new ActiveXObject("WScript.Shell")
        if (wscript != null) {
            wscript.SendKeys("{F11}")
        }
    }
}