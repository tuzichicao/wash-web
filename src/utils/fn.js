/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-15 14:24:36
 * @LastEditTime: 2021-01-15 14:30:19
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/utils/fn.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/* eslint-disable */

export const fn = {
    debounce: (f, args = null, delay = 1000) => {
        let timer = null
        return () => {
            if (timer) {
                clearTimeout(timer)
                timer = null
            }
            timer = setTimeout(() => {
                f()
            }, delay)
        }
    }
}
