/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-28 17:53:15
 * @LastEditTime: 2021-01-08 17:20:30
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-merchant/src/utils/oss-lib.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */


import axios from 'axios'
import { appConfig } from '@/config/app'

const openUrl = process.env.NODE_ENV === 'production' ? appConfig.openUrl.prod : appConfig.openUrl.dev
export class OssLib {

    constructor() {
        this.axios = axios
    }

    _openFile() {
        return new Promise((resolve) => {
            var pom = document.createElement('input')
            pom.setAttribute('type', 'file')
            pom.addEventListener('change', function (e) {
                resolve(e)
            })
            pom.style.display = 'none'
            if (document.createEvent) {
                const event = document.createEvent('MouseEvents')
                event.initEvent('click', true, true)
                pom.dispatchEvent(event)
            } else {
                pom.click()
            }
        })
    }

    _generateKey() {
        const chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
        const maxPos = chars.length
        let name = ''
        for (let i = 0; i < 7; i++) {
            name += chars.charAt(Math.floor(Math.random() * maxPos))
        }
        return `${name}${new Date().getTime()}`
    }

    _getAuthorize() {
        return new Promise((resolve) => {
            this.axios.post(`${openUrl}open/oss/authorize`).then(res => {
                const { data } = res
                if (data.status) {
                    resolve(data.data)
                }
            })
        })
    }

    _post(authorize, file) {
        return new Promise((resolve) => {
            const formData = new FormData()
            formData.append('policy', authorize.policy)
            formData.append('OSSAccessKeyId', authorize.accessKey)
            formData.append('success_action_status', '200')
            formData.append('callback', authorize.callback)
            formData.append('signature', authorize.signature)
            formData.append('key', authorize.dir + this._generateKey())
            formData.append('dir', authorize.dir);
            formData.append('file', file)
            this.axios({
                url: '//' + authorize.host,
                method: 'post',
                data: formData,
                headers: { 'Content-Type': 'multipart/form-data' }
            }).then(res => {
                resolve(res)
            })
        })
    }

    uploadLocalFile() {
        return new Promise((resolve) => {
            this._openFile().then(e => {
                const file = e.path[0].files[0]
                this._getAuthorize().then(authorize => {
                    this._post(authorize, file).then(res => {
                        const { status, data } = res
                        if (status === 200) {
                            resolve(data)
                        }
                    })
                })
            })
        })
    }

    uploadFile(file) {
        return new Promise((resolve) => {
            this._getAuthorize().then(authorize => {
                this._post(authorize, file).then(res => {
                    resolve(res)
                })
            })
        })
    }
}