/*
 * @Author: 码上talk|RC
 * @Date: 2020-10-09 16:09:14
 * @LastEditTime: 2020-10-09 17:04:57
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-merchant/src/utils/history-bar.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/*eslint-disable */
const HISTORY_LS_KEY = 'HISTORY_LS_KEY'

export const historyBar = {
    add: (o) => {
        let arrHis = localStorage.getItem(HISTORY_LS_KEY) ? JSON.parse(localStorage.getItem(HISTORY_LS_KEY)) : []
        if (arrHis.map(i => i.name).includes(o.name)) {
            arrHis.splice(arrHis.map(i => i.name).indexOf(o.name), 1)
        }
        arrHis.push(o)
        localStorage.setItem(HISTORY_LS_KEY, JSON.stringify(arrHis))
    },
    delete: (index) => {
        let arrHis = localStorage.getItem(HISTORY_LS_KEY) ? JSON.parse(localStorage.getItem(HISTORY_LS_KEY)) : []
        arrHis.splice(index, 1)
        localStorage.setItem(HISTORY_LS_KEY, JSON.stringify(arrHis))
    },
    all: () => {
        return localStorage.getItem(HISTORY_LS_KEY) ? JSON.parse(localStorage.getItem(HISTORY_LS_KEY)) : []
    }
}