/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-16 16:20:41
 * @LastEditTime: 2021-01-22 10:36:10
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-admin/src/utils/form-toolkit.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
const _ = require('lodash')

import { GoodsItem } from '@/model/goods/item'
import { MerchantUser } from '@/model/merchant/user'

const models = {
    GoodsItem,
    MerchantUser
}

export const formToolkit = {
    gen: (obj) => {
        let form = {}
        for (let i in obj) {
            if (Array.isArray(obj[i]['value'])) {
                obj[i]['value'].forEach(j => {
                    if (!form[obj[i]['key']]) {
                        form[obj[i]['key']] = []
                    }
                    form[obj[i]['key']].push(formToolkit.gen(j))
                })
                continue
            }
            form[obj[i]['key']] = _.cloneDeep(obj[i]['value'])
        }
        return form
    },
    fill: (form, data) => {
        let f = _.cloneDeep(form)
        for (let i in f) {
            if (Array.isArray(f[i]['value'])) {
                data[f[i]['key']].forEach(j => {
                    const model = f[i]['type'].split('>')[1]
                    f[i]['value'].push(formToolkit.fill(models[model].FORM.init('update'), j))
                })
                continue
            }
            f[i]['value'] = _.cloneDeep(data[f[i]['key']])
        }
        return f
    }
}
