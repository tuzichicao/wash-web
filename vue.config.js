/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:23:29
 * @LastEditTime: 2021-01-15 15:36:43
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-admin/vue.config.js
 * @Just do what I think it is right
 */
module.exports = {
    devServer: {
        proxy: {
            '/admin': {
                target: 'http://localhost:4001',
                ws: false,
                changeOrigin: true
            },
            '/open': {
                target: 'http://localhost:4003',
                ws: false,
                changeOrigin: true
            }
        }
    },
    chainWebpack: config => {
        config.module
            .rule('vue')
            .test(/\.vue$/)
            .use('iview-loader')
            .loader('iview-loader')
            .options({
                prefix: true
            })
    }
}
